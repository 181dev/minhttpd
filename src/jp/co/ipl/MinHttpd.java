/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.co.ipl;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.nio.file.Files;

/**
 *
 * @author iwai
 */
public class MinHttpd {

    private static final int PORT = 8880;

    public static void main(String[] args) throws IOException {
        HttpServer server = HttpServer.create(new InetSocketAddress("localhost", PORT), 0);
        server.createContext("/", new HttpHandler() {
            @Override
            public void handle(HttpExchange he) throws IOException {
                String path = he.getRequestURI().getPath();
                File f = new File(".\\", path);
                if (!f.exists()) {
                    //ファイルがない
                    he.sendResponseHeaders(404, 0);
                    sendMessage(he.getResponseBody(), "File Not Found.");
                    return;
                }
                if (f.isDirectory()) {
                    File indexFile = new File(f, "index.html");
                    if (!indexFile.exists()) {
                        //ディレクトリは見れない
                        he.sendResponseHeaders(403, 0);
                        sendMessage(he.getResponseBody(), "forbidden.");
                        return;
                    }
                    f = indexFile;
                }
                he.getResponseHeaders().add("Content-Type", Files.probeContentType(f.toPath()));
                he.sendResponseHeaders(200, f.length());
                //ファイル内容を返す
                InputStream is = new FileInputStream(f);
                try (OutputStream res = he.getResponseBody()) {
                    Files.copy(f.toPath(), res);
                } finally {
                    is.close();
                }
            }
        });
        server.start();
    }

    private static void sendMessage(OutputStream res, String message) throws IOException {
        try {
            try (PrintWriter pw = new PrintWriter(res)) {
                pw.println(message);
            }
        } finally {
            res.close();
        }
    }
}
